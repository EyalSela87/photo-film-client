import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Response } from '../modals/response.modal';
import { LogsService } from './logs.service';

import { ImageInfo } from '../modals/imageInfo.modal';

@Injectable({
  providedIn: 'root'
})
export class CameraInfoService
{
  readonly BASE_URL: string = 'https://404040.co.il/firumu2/cameraInfo';
  // readonly BASE_URL: string = 'http://localhost:3000/cameraInfo';
  // readonly BASE_URL: string = "https://10.0.2.2:3000/cameraInfo";

  public cameras: string[];
  public films: string[];
  public copiedImageInfo: ImageInfo;

  constructor(private http: HttpClient, private LogService: LogsService)
  {
    console.log("[CameraInfoService] => (constructor)");
    this.getAllCameras();
    this.getAllFilms();

  }


  private getAllCameras = () =>
  {
    this.http.get<Response>(this.BASE_URL + '/getCameras')
      .subscribe(result =>
      {
        console.log("#######", result);
        const camerasNames: string[] = [];
        camerasNames.push("Add New Camera");
        result.data['cameraName'].map(c => camerasNames.push(c['name']))
        this.cameras = camerasNames;
      }, err => this.LogService.throwError(err['error']['error'].split(',')));
  }

  private getAllFilms = () =>
  {
    this.http.get<Response>(this.BASE_URL + '/getFilms')
      .subscribe(result =>
      {
        const filmsNames: string[] = []
        filmsNames.push("Add New Film");
        result.data['filmName'].map(f => filmsNames.push(f['name']));
        this.films = filmsNames;
      })
  }

  public addNewCamera = (cameraName: string) => this.addCameraToDb(cameraName);

  public addNewCameras = (camerasNames: string[]) =>
  {
    this.http.post<Response>(this.BASE_URL + '/addmayCameras', { cameras: camerasNames })
      .subscribe(result => console.log("[CameraInfoService] => (addNewCameras) result:", result),
        err => this.LogService.throwError(err['error']['error'].split(',')));
  }

  private addCameraToDb = (cameraName: string) =>
  {
    this.http.post<Response>(this.BASE_URL + '/addCamera', { name: cameraName })
      .subscribe(result => { this.cameras.push(cameraName); },
        err => this.LogService.throwError(err['error']['error'].split(',')));
  }

  public addNewFilm = (filmName: string) => this.addFilmToDb(filmName);

  public addNewfilms = (filmsNames: string[]) =>
  {
    this.http.post<Response>(this.BASE_URL + '/addmanyFilms', { films: filmsNames })
      .subscribe(result => console.log("[CameraInfoService] => (addNewCameras) result:", result),
        err => this.LogService.throwError(err['error']['error'].split(',')));
  }


  private addFilmToDb = (filmName: string) =>
  {
    this.http.post<Response>(this.BASE_URL + '/addFilm', { name: filmName })
      .subscribe(result =>
      {
        this.films.push(filmName);
      }, err => this.LogService.throwError(err['error']['error'].split(',')));
  }

  public onCopyImageValue = (values: ImageInfo) =>
  {
    if (!this.copiedImageInfo)
    {
      this.copiedImageInfo = new ImageInfo(values.caption, values.camera, values.lense, values.isoRate, values.f_stop,
        values.sutterSpeed, values.overExpose, values.avg_light_shadow, values.film);
      return;
    }
    this.copiedImageInfo.caption = values.caption
    this.copiedImageInfo.camera = values.camera
    this.copiedImageInfo.lense = values.lense
    this.copiedImageInfo.isoRate = values.isoRate
    this.copiedImageInfo.f_stop = values.f_stop
    this.copiedImageInfo.sutterSpeed = values.sutterSpeed
    this.copiedImageInfo.overExpose = values.overExpose
    this.copiedImageInfo.avg_light_shadow = values.avg_light_shadow
    this.copiedImageInfo.film = values.film
  }

  public onResteImageValue = () => this.copiedImageInfo = null;

}
