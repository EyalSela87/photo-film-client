import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { UsersService } from '../../services/users.service';
import { LogsService } from '../../services/logs.service';
import { UserInfo } from '../../modals/userInfo.modal';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit
{
  public loading: boolean = false;
  public loginInfo: UserInfo = new UserInfo("", "", "", "");
  private authChangeSub: Subscription;


  constructor(
    private userService: UsersService,
    private logsService: LogsService,
    private toastController: ToastController,
    private router: Router) { }

  ngOnInit()
  {
    this.logsService.onError.subscribe((massage: string[]) => { this.setTosterAlert("dark", massage); if (this.loading) this.loading = false });
  }

  ionViewWillEnter()
  {
    if (this.userService.isUserLogged()) this.router.navigate(['/main']);
    this.authChangeSub = this.userService.onAuthChanged.subscribe(() =>
    {
      this.loading = false
      if (this.userService.isUserLogged()) this.router.navigate(['/main']);
    })
  }

  ionViewWillLeave()
  {
    this.authChangeSub.unsubscribe();
  }

  private async setTosterAlert(color: string, errors: string[], mili_sec: number = 3000)
  {// popup alets with following errors
    const toast = await this.toastController.create({
      message: errors.join('\n\n'),
      position: 'top',
      color: color,
      duration: mili_sec
    });
    toast.present();
  }

  onSubmit = () =>
  {
    const errors = this.verifyLoginInfo(this.loginInfo);

    if (errors.length > 0)
      this.setTosterAlert("dark", errors);
    else
    {
      this.loading = true;
      this.userService.loginUser(this.loginInfo);
    }
  }

  onSubmit1 = () =>
  {

    this.userService.test();
    return;
    const errors = this.verifyLoginInfo(this.loginInfo);

    if (errors.length > 0)
      this.setTosterAlert("dark", errors);
    else
      this.userService.loginUser(this.loginInfo);
  }

  verifyLoginInfo = (info: UserInfo): string[] =>
  {// verify login information
    const errors: string[] = [];

    if (info.email === "") errors.push("email field is require");
    else if (!this.validateEmail(info.email)) errors.push("email is not valid");

    if (info.password === "") errors.push("password field is require");
    else
    {
      const noSpacePassword: string = info.password.split(' ').join('');
      if (noSpacePassword.length < 5) errors.push("password must cointain atleast 5 characters & no spaces");
    }

    return errors;
  }

  validateEmail = (email) =>
  {// validate email
    const check = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return check.test(String(email).toLowerCase());
  }

}
