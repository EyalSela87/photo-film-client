import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
// import { MainService } from '../../services/user.service';
import { UsersService } from '../../services/users.service';
import { LogsService } from '../../services/logs.service'

import { UserInfo } from '../../modals/userInfo.modal';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit
{
  public loading: boolean = false;
  public registerInfo: UserInfo = new UserInfo("", "", "", "");
  private authChangeSub: Subscription;

  constructor(
    private navCtrl: NavController,
    private toastController: ToastController,
    private userService: UsersService,
    private logsService: LogsService,
    private router: Router
  )
  { }

  ngOnInit()
  {
    this.logsService.onError.subscribe((errors: string[]) => 
    {
      this.loading = false;
      this.setTosterAlert("dark", errors)
    });

  }

  ionViewWillEnter()
  {
    if (this.userService.isUserLogged()) this.router.navigate(['/main']);
    this.authChangeSub = this.userService.onAuthChanged.subscribe(() => 
    {
      this.loading = false;
      if (this.userService.isUserLogged()) this.router.navigate(['/main']);
    })
  }

  ionViewWillLeave()
  {
    this.authChangeSub.unsubscribe();
  }

  async setTosterAlert(color: string, errors: string[], mili_sec: number = 3000)
  {// popup alets with following errors
    const toast = await this.toastController.create({
      message: errors.join('\n\n'),
      position: 'top',
      color: color,
      duration: mili_sec
    });
    toast.present();
  }

  public navigateToLoadingPage = () => this.navCtrl.navigateBack(['/login']);

  public onSubmit = () =>
  {
    const errors: string[] = this.verifyRegisterInfo(this.registerInfo);

    if (errors.length > 0) this.setTosterAlert("dark", errors);
    else
    {
      this.loading = true;
      this.userService.createNewUser(this.registerInfo);
    }
  }

  private verifyRegisterInfo = (info: UserInfo): string[] =>
  {// verify register information
    const errors: string[] = [];

    if (info.name === "") errors.push("name field is require");
    else
    {
      const noSpaceName: string = info.name.split(' ').join('');
      if (noSpaceName.length < 3) errors.push("name must cointain atleast 3 characters");
    }

    if (info.email === "") errors.push("email field is require");
    else if (!this.validateEmail(info.email)) errors.push("email is not valid");

    if (info.password === "") errors.push("password field is require");
    else
    {
      const noSpacePassword: string = info.password.split(' ').join('');
      if (noSpacePassword.length < 5) errors.push("password must cointain atleast 5 characters & no spaces");
    }

    if (info.password !== info.verifyPassword) errors.push("passwords are not match");

    return errors;
  }

  private validateEmail = (email) =>
  {// validate email
    const check = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return check.test(String(email).toLowerCase());
  }

}
