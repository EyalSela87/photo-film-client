import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import Swal from 'sweetalert2';
import { Subscription } from 'rxjs';

import { UsersService } from '../../services/users.service';
import { AlbumsService } from '../../services/albums.service';
import { LogsService } from '../../services/logs.service';

import { Inject, Injectable } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit
{
  public loading: boolean = true;
  public showOptions: boolean = false;
  private authChangedSub: Subscription;

  constructor(
    private userService: UsersService,
    public albumsService: AlbumsService,
    private logsService: LogsService,
    private navCtrl: NavController,
    private toastController: ToastController
  ) { }

  ngOnInit()
  {
    this.logsService.onError.subscribe((errors: string[]) =>
    {
      this.setTosterAlert('dark', errors);
      this.loading = false;
    });
  }

  ionViewWillEnter()
  {
    if (!this.userService.isUserLogged()) this.navCtrl.navigateBack(['/login']);

    this.albumsService.albumsHasLoaded.subscribe(() => (this.loading = false));
    this.albumsService.albumHasCreated.subscribe(() => (this.loading = false));

    this.authChangedSub = this.userService.onAuthChanged.subscribe(() =>
    {
      if (!this.userService.isUserLogged())
        this.navCtrl.navigateBack(['/login']);
    });

    if (this.albumsService.albums !== null) this.loading = false;
    if (!this.albumsService.albums)
      this.albumsService.getAllbumsFromDb(this.userService.getUserId());
  }

  ionViewWillLeave()
  {
    this.authChangedSub.unsubscribe();
  }

  async setTosterAlert(
    color: string,
    errors: string[],
    mili_sec: number = 3000
  )
  {
    // popup alets with following errors
    const toast = await this.toastController.create({
      message: errors.join('\n\n'),
      position: 'top',
      color: color,
      duration: mili_sec,
    });
    toast.present();
  }

  private validateAlbumName = (name: string): string[] =>
  {
    const errors: string[] = [];

    if (name === '') errors.push('please enter album name');
    else
    {
      const noSpaceName = name.split(' ').join('');
      if (noSpaceName.length < 3)
        errors.push('name must contain at least 3 charecters');
    }

    return errors;
  };

  public onOpenModal = async () =>
  {
    console.log('(onOpenModal)');

    const { value: albumName } = await Swal.fire({
      title: 'Album Creation',
      input: 'text',
      inputLabel: 'Please enter album name',
      inputPlaceholder: 'album name',
      heightAuto: false,
    });

    if (albumName)
    {
      const errors: string[] = this.validateAlbumName(albumName);

      if (errors.length > 0) this.setTosterAlert('dark', errors);
      else
      {
        this.loading = true;
        this.albumsService.createNewAlbum(albumName);
      }
    } else if (albumName === '')
      this.setTosterAlert('dark', ['name must contain at least 3 charecters']);
  };

  public onAlbumClicked = (albumId: number) =>
  {
    this.albumsService.onAlbumSelected(albumId);
    this.navCtrl.navigateForward(['/album', albumId]);
  };

  public test = () =>
  {
    // this.albumsService.sendTest();
  };

  public onShowOptions = (show: boolean) => this.showOptions = show;

  public onBackButton = () => this.navCtrl.navigateBack(['/login']);

  public onLogout = () =>
  {
    this.userService.logoutUser();
    this.navCtrl.navigateBack(['/']);
  }
}
