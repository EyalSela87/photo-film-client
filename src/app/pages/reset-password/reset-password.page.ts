import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';

import { LogsService } from '../../services/logs.service';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit
{

  public email: string = "";

  constructor(
    private navCtrl: NavController,
    private toastController: ToastController,
    private userService: UsersService,
    private logsService: LogsService
  ) { }

  ngOnInit()
  {
    this.logsService.onError.subscribe((massage: string[]) => { this.setTosterAlert("dark", massage); });
  }




  private async setTosterAlert(color: string, errors: string[], mili_sec: number = 3000)
  {// popup alets with following errors
    const toast = await this.toastController.create({
      message: errors.join('\n\n'),
      position: 'top',
      color: color,
      duration: mili_sec
    });
    toast.present();
  }

  public onSubmit = () =>
  {
    const errors: string[] = [];
    // errors validation
    if (this.email === "") errors.push("email field is require");
    else if (!this.validateEmail(this.email)) errors.push("email is not valid");
    // if there is an error show it to user and return
    if (errors.length > 0)
      return this.setTosterAlert("dark", errors);

    this.userService.resetUserPassword(this.email);
  }

  public onBackButton = () => this.navCtrl.navigateBack(['/']);


  private validateEmail = (email) =>
  {// validate email
    const check = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return check.test(String(email).toLowerCase());
  }
}
