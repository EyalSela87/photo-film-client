import { Injectable, Output, EventEmitter } from '@angular/core';

import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogsService
{

  @Output() onError = new EventEmitter<string[]>();

  constructor() { }


  public throwError = (errors: string[]) => 
  {
    console.log("ok");
    this.onError.emit(errors);
  }


}