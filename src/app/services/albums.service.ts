import { Injectable, EventEmitter, Output } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Subject } from 'rxjs';

import { UsersService } from './users.service';
import { LogsService } from './logs.service';
import { Album } from '../modals/album.modal';
import { ImageInfo } from '../modals/imageInfo.modal';
import { Response } from '../modals/response.modal';

@Injectable({
  providedIn: 'root',
})
export class AlbumsService
{
  @Output() onCreateImageCompleted = new EventEmitter<void>();
  @Output() onImageAdded = new EventEmitter<void>();

  private readonly BASE_URL: string = 'https://404040.co.il/firumu2/albums';
  private readonly PHP_URL: string = "https://www.tapper.org.il/647/laravel/public/api/AddImage";
  private readonly BASE_IMAGE_PATH: string = "https://www.tapper.org.il/647/laravel/public/uploads/";

  public albums: Album[] = null;
  public currentAlbum: Album = null;

  // public onAlbumsUpdate = new Subject<Album[]>();
  // public onAlbumDetailReceived = new Subject<Album>();
  public onImagesUpdate = new Subject<ImageInfo[]>();
  public albumsHasLoaded = new EventEmitter<void>();
  public albumHasCreated = new EventEmitter<void>();

  constructor(
    private http: HttpClient,
    private userService: UsersService,
    private logsService: LogsService,
    private storage: AngularFireStorage
  ) { }

  public getAlbums = (): Album[] => this.albums;

  public getAllbumsFromDb = (userId: number) =>
  {
    if (this.albums !== null) return;
    if (userId === null) return;

    this.http.get<Response>(this.BASE_URL + '/getAlbums/' + userId).subscribe(
      (res) =>
      {
        if (!res.succeed) this.logsService.throwError(res.error.split(','));
        console.log(res.data);
        const albums: Album[] = [];

        res.data['albums'].map((album) =>
        {
          albums.push(new Album(album['name'], [], album['coverImgIndex'], album['id']));
        });

        this.albums = albums;
        this.albumsHasLoaded.emit();
      },
      (err) => this.logsService.throwError(err['error']['error'].split(','))
    );
  };

  public createNewAlbum = (albumName: string) =>
  {
    const data = {
      id: this.userService.getUserId(),
      name: albumName,
    };

    if (data.id === null || data.name === null)
      return this.logsService.throwError(['userId or Album name is not set']);

    this.http.post<Response>(this.BASE_URL + '/create', data).subscribe(
      (res) =>
      {
        if (!res.succeed)
          return this.logsService.throwError(res.error.split(','));
        this.albums.push(new Album(res.data['name'], [], 0, res.data['id']));
        // this.onAlbumsUpdate.next(this.albums);
        console.log(res.data);
        this.albumHasCreated.emit();
      },
      (err) => this.logsService.throwError(err['error']['error'].split(','))
    );
    console.log('[AlbumsService] => (createNewAlbum)', albumName);
  };

  public onAlbumSelected = (albumId: number) =>
  {
    // find the album by id
    const album: Album = this.albums.find((a) => a.id === albumId);
    this.currentAlbum = album;
    // get all thte images of the album
    this.http.get<Response>(this.BASE_URL + '/getImages/' + album.id).subscribe(
      (res) =>
      {
        if (!res.succeed) this.logsService.throwError([res.error]);

        const images: ImageInfo[] = [];
        const data = res.data['images'];
        console.log('[AlbumsService] => (onAlbumSelected) data', data);
        data.map((d) =>
        {
          images.push(
            new ImageInfo(
              d['caption'],
              d['camera'],
              d['lense'],
              d['isoRate'],
              d['f_stop'],
              d['sutterSpeed'],
              d['overExpose'],
              d['avg_light_shadow'],
              d['film'],
              d['url']
            )
          );
        });

        this.currentAlbum.images = images;
        this.onImagesUpdate.next(images);
        console.log('[AlbumsService] => (onAlbumSelected)', images);
      },
      (err) => this.logsService.throwError(err['error']['error'])
    );
  };

  public getCurrentAlbum = (): Album => this.currentAlbum;

  public createNewImage = (image: File, info: ImageInfo) =>
  {

    this.storage.upload(`images/${Date.now()}-${image.name}`, image)
      .then((result: any) => result.ref.getDownloadURL())
      .then((imageUrl: string) => this.setImageData(info, image.name, imageUrl))
      .catch(err => console.log("err", err))
  };

  private setImageData(info: ImageInfo, imageName: string, imageUrl: string)
  {
    const data = {
      caption: info.caption,
      camera: info.camera,
      lense: info.lense,
      isoRate: info.isoRate,
      f_stop: info.f_stop,
      sutterSpeed: info.sutterSpeed,
      overExpose: info.overExpose,
      avg_light_shadow: info.avg_light_shadow,
      film: info.film,
      imageUrl: imageUrl,
      imageName: imageName,
      albumId: this.currentAlbum.id,
    };

    this.http.post<Response>(this.BASE_URL + '/createImage', data)
      .subscribe(res =>
      {
        if (!res.succeed) return this.logsService.throwError([res.error]);
        this.onCreateImageCompleted.emit();
        const image: ImageInfo = this.setImageInfo(res.data);
        this.currentAlbum.images.push(image);
        this.onImagesUpdate.next(this.currentAlbum.images);
        this.onImageAdded.emit();
      }, err =>
      {
        console.log(err);
        this.logsService.throwError([err]);
      });
  }

  private getHeaders = (): HttpHeaders =>
  {
    return new HttpHeaders({
      Authorization: 'my-request-token',
      'X-Requested-With': 'HttpClient',
      'Content-Type': 'multipart/form-data',
    });
  };

  private setImageInfo = (data: any): ImageInfo =>
  {
    return new ImageInfo(
      data['caption'],
      data['camera'],
      data['lense'],
      data['isoRate'],
      data['f_stop'],
      data['sutterSpeed'],
      data['overExpose'],
      data['avg_light_shadow'],
      data['film'],
      data['url']);
  };

}
