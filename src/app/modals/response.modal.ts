export class Response
{
    succeed: boolean;
    data: object;
    error: string;
}