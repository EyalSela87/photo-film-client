import { Component, OnInit, Input } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { CameraInfoService } from '../../services/camera-info.service';
// import { Subscription } from 'rxjs';

import { Album } from '../../modals/album.modal';
import { ImageInfo } from '../../modals/imageInfo.modal';
import { AlbumsService } from '../../services/albums.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit
{
  // public readonly cameras: string[] = ["Agfa Optima 1a", "Agfa Optima Flash Sensor", "Alpa-Reflex Camera", "Argus C3", "Asahi Pentax"];
  public readonly cameras: string[] = [
    "Agfa Optima 1a", "Agfa Optima Flash Sensor", "Alpa-Reflex Camera", "Argus C3", "Asahi Pentax",
    "Asahiflex", "Balda Baldessa", "Calypso", "Canon Canonflex", "Canon Pellix",
    "Chinon Genesis IV", "Contarex", "Contax T", "Corfield Periflex", "Cosina CT-1",
    "Yashica Electro 35", "Franka Kamerawerk", "Fuji TX-1", "Fuji TX-2", "Hasselblad XPan",
    "Hasselblad XPan II", "Kine Exakta", "Kodak 35", "Kodak 35 Rangefinder", "Kodak Retina",
    "Kodak Retina IIIS", "Kodak Retina Reflex III", "Kodak Retina Reflex IV", "Kodak Retina Reflex S", "Kodak Retinette",
    "Konica C35 AF", "Leica CL", "Leica copies", "Leica R-E", "Leica R3",
    "Leica R4", "Leica R4s", "Leica R5", "Leica R6", "Leica R6.2",
    "Leica R7", "Leica R8", "Leica R9", "Leica RE", "Leica Standard",
    "Leicaflex / SL / SL2", "Leidolf", "Leitz Leicaflex", "Leitz Leicaflex SL", "Leitz Leicaflex SL Mot",
    "Leitz Leicaflex SL2", "Leitz Leicaflex SL2 Mot", "Leitz Minolta CL", "Yashica Lynx", "Mecaflex",
    "Minolta 9xi", "Minolta 35", "Minolta 5000i", "Minolta 7000i", "Minolta Maxxum 4",
    "Minolta Maxxum 7000", "Minolta Maxxum 9000", "Minolta SR-2", "Minolta SR-T 101", "Minolta TC-1",
    "Minolta X-1", "Minolta X-570", "Minolta X-700", "Minolta XD-7", "Minolta XE",
    "Minolta XE-5", "Minolta XG-M", "Miranda T", "Neoca 2s", "Nikon",
    "Nikon FE", "Nikon FE2", "Olympus Pen F", "Olympus Superzoom 120TC", "Olympus XA",
    "Pentax K1000", "Pentax LX", "Pentax ME", "Pentax ME F", "Pentax ME Super",
    "Pentax MG", "Pentax MX", "Pentax MZ-S", "Pentax SF7", "Pentax SFX",
    "Pentax Spotmatic", "Pentax Super-A", "Pentax ZX-50", "Praktiflex", "Tenax II",
    "Tessina", "Topcon RE Super", "Voigtländer Bessamatic", "Agfa Clack", "Ami",
    "Bronica", "Diana", "Flexaret", "Hasselblad", "Holga",
    "Lubitel", "Mamiya 6", "Mamiya 7", "Mamiya 645", "Mamiya C",
    "Mamiya C220", "Mamiya C330", "Mamiya Press", "Mamiya RZ67", "Mamiya Six",
    "Mamiyaflex", "Omega", "Pentacon Six", "Pentax 6×7", "Pentax 67",
    "Pentax 67II", "Pentax 645", "Pentax 645N", "Pentax 645NII", "Plaubel Makina",
    "Rolleicord", "Rolleiflex", "Start (Polish camera)", "Voigtländer Brillant",
    "Other"
  ]
  public readonly films: string[] =
    [
      "Kodak portra 160", "Kodak portra 400", "Kodak portra 800", "Kodak ektar 100", "Kodak Ultramax 400",
      "Kodak Gold 200", "Kodak Pro Image 100", "Kodak ColorPlus 200", "Kodak Ektachrome E100", "Kodak Ultramax 400",
      "Kodak T-MAX P3200", "Kodak T-MAX 400", "Kodak T-MAX 100", "Kodak Tri-X", "Lomography Potsdam 100", "Lomography Berlin 400", "Lomography Color Negative 100",
      "Lomography Color Negative 400", "Lomography Color Negative 800", "Lomography Color Tiger  200", "Lomography Redscale XR 50-200", "Lomography LomoChrome Metropolis",
      "Lomography LomoChrome Purple XR", "Lomography Lobster Redscale", "Lomography XPro 200", "Lomography Peacock X-Pro",
      "ADOX CMS 20 II PRO", "ADOX HR-50", "ADOX IR-HR PRO 50", "ADOX CHS 100 II", "ADOX Silvermax", "ADOX SCALA 50", "ADOX SCALA 160",
      "Argenti Copex Rapid", "Argenti APX 100", "Argenti APX 400", "Argenti Nanotomic X", "Argenti Scale-X", "Argenti PAN-X", "Argenti ARF+ Reporter Film Plus",
      "Bergger Pancro 400",
      "CatLABS X FILM 80", "CatLABS X FILM 320",
      "Cinestill bwXX", "Cinestill 50D", "Cinestill 800T",
      "DubblefilmApollo", "DubblefilmBubblegum", "DubblefilmJelly", "DubblefilmPacific", "DubblefilmSolar", "DubblefilmStereo",
      "Ferrania P30",
      "Foma FOMAPAN 100 'Classic", "Foma FOMAPAN 200 'Foma Creative", "Foma FOMAPAN 400 'Foma Creative", "Foma RETRO PAN 320 'Foma Soft", "Foma FOMAPAN R 100",
      "FOTOIMPEX CHM 100", "FOTOIMPEX CHM 400",
      "FUJIFILM Neopan ACROS 100 II", "FUJIFILM Neopan 400CN", "FUJIFILM FujiColor 100", "FUJIFILM FujiColor Superia Premium 400", "FUJIFILM FujiColor C200",
      "FUJIFILM FujiColor Superia X-tra 400", "FUJIFILM Fujicolor Pro 160NS", "FUJIFILM Fujicolor Pro 400H", "FUJIFILM Fujichrome Velvia 100",
      "FUJIFILM Fujichrome Velvia 100F", "FUJIFILM Fujichrome Provia 100F",
      "HOLGA 400",
      "ILFORD Pan 100", "ILFORD Pan 400", "ILFORD Pan F Plus", "ILFORD FP4 Plus", "ILFORD HP5 Plus", "ILFORD DELTA 100", "ILFORD DELTA 400", "ILFORD DELTA 3200",
      "ORTHO_PLUS SFX 200", "ORTHO_PLUS XP2 Super",
      "JCH Streetpan 400",
      "Kentmere PAN 100", "Kentmere PAN 400",
      "KONO Donau 6", "KONO Kolorit 125 Tungsten", "KONO Kolorit 400 Tungsten", "KONO Rotwild 400", "KONO ALiEN 200",
      "KONO UFO 200", "KONO KATZ 200", "KONO LUFT 200", "KONO LIEBE 200", "KONO WINTERMÄRCHEN 200",
      "Oriental SEAGULL 100", "Oriental SEAGULL 400",
      "Rera Pan 400", "Rera Chrome 100",
      "Revolog Rasp", "Revolog Volvox", "Revolog Texture", "Revolog Plexus", "Revolog Laser", "Revolog Streak", "Revolog Tesla I",
      "Revolog Tesla II", "Revolog Kosmos", "Revolog Kolor", "Revolog nm 460", "Revolog nm 600",
      "Rollei RPX 25", "Rollei RPX 100", "Rollei RPX 400", "Rollei Ortho Plus", "Rollei Retro 80S", "Rollei SuperPan 200",
      "Rollei Retro 400S", "Rollei Infrared 400", "Rollei Blackbird", "Rollei Redbird", "Rollei Redbird",
      "Shanghai GP3 100 PAN",
      "Silberra S25", "Silberra Orta 50", "Silberra Orta 100", "Silberra Pan 50/Ultima 50", "Silberra Pan 160/Ultima 160", "Silberra Pan 200/Ultima 200",
      "Silberra Ultima 100", "Silberra U200", "Silberra U400", "Silberra Cinema UN54", "Silberra Cinema+ 74N", "Silberra Cinema+ 75N", "Silberra Cinema 52XX",
      "SPUR DSX", "SPUR UR", "SPUR Ultra R 800",
      "Street_Candy ATM 400",
      "Svema_Astrum МЗ-3 (MZ-3)", "Svema_Astrum H-64 (FN-64)", "Svema_Astrum Foto-100", "Svema_Astrum Foto-200", "Svema_Astrum Foto-400",
      "Svema_Astrum A-2SH", "Svema_Astrum NK-2", "Svema_Astrum Color",
      "Ultrafine Xtreme 100", "Ultrafine Xtreme 400",
      "Yodica Antares", "Yodica Andromeda", "Yodica Atlas", "Yodica Pegasus", "Yodica Polaris", "Yodica Sirio", "Yodica Vega",
      "Other"
    ]
  // public readonly avg_light_shadow: string[] = ["Metered for light", "Metered for shadow", "Average"]
  public readonly avg_light_shadow: string[] = ["Light", "Shadow", "Average"];
  public readonly expose: string[] = ["Under expose", "Over expose", "box speed"];

  public other: { camera: string, film: string } = { camera: "", film: "" };

  @Input() albumInfo: Album;
  private picture: File = null;
  private image: File;
  public loading: boolean = false;
  public showBackdrop: boolean = false;
  // public onCopyOrPasteValues: boolean = false;
  public onCopy: boolean = false;
  public onPaste: boolean = false;
  @Input() imageInfo: ImageInfo = new ImageInfo("", "", "", "", "", "", "", "", "");
  @Input() viewOnly: boolean = false;
  // private imageCreatedSub:Subscription;

  public x: string = "";

  constructor(
    private albumsService: AlbumsService,
    private modalCtrl: ModalController,
    private toastController: ToastController,
    public cameraInfoService: CameraInfoService)
  {

  }

  ngOnInit()
  {
    this.albumsService.onCreateImageCompleted.subscribe(() => this.loading = false);
    this.albumsService.onImageAdded.subscribe(() => this.close());
  }

  async setTosterAlert(color: string, errors: string[], mili_sec: number = 3000)
  {// popup alets with following errors
    const toast = await this.toastController.create({
      message: errors.join('\n\n'),
      position: 'top',
      color: color,
      duration: mili_sec
    });
    toast.present();
  }

  ionViewWillEnter()
  {

  }

  ionViewWillLeave()
  {

  }

  public close = () =>
  {
    this.modalCtrl.dismiss();
  }

  public onSubmit = () =>
  {
    if (!this.image) return this.setTosterAlert("dark", ['No picture been selected']);
    else if (this.image === null) return this.setTosterAlert("dark", ['No picture been selected']);


    if (this.imageInfo.film === "Add New Film")
    {
      if (this.other.film === "") return this.setTosterAlert("dark", ['No film has been set']);
      this.cameraInfoService.addNewFilm(this.other.film)
      this.imageInfo.film = this.other.film;
    }

    else if (this.imageInfo.film === "") return this.setTosterAlert("dark", ['No film has been set']);

    if (this.imageInfo.camera === "Add New Camera")
    {
      if (this.other.camera === "") return this.setTosterAlert("dark", ['No camera has been set']);
      this.cameraInfoService.addNewCamera(this.other.camera);
      this.imageInfo.camera = this.other.camera;
    }
    else if (this.imageInfo.camera === "") return this.setTosterAlert("dark", ['No camera has been set']);

    this.loading = true;

    // console.log("#######")
    // console.log(this.image);
    // console.log("#######")
    this.albumsService.createNewImage(this.image, this.imageInfo);
  }

  public onFileChange = (event: any) =>
  {
    // this.image = null;
    // this.picture = null;
    // this.picture = event.target['files'][0];

    // //#region  SHAI CODE
    // const file: File = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
    // const pattern: any = /image-*/;
    // const reader: any = new FileReader();

    // if (!file.type.match(pattern))
    // {
    //   alert('invalid format');
    //   return;
    // }
    // this.image = reader.result;
    // reader.onload = this._handleReaderLoaded.bind(this);
    // reader.readAsDataURL(file);

    this.image = event.target['files'][0];
    console.log(this.image);


    //#endregion
  }

  private _handleReaderLoaded(e: any)
  {// SHAI FUNCTION
    console.log("123")
    const reader: any = e.target;
    this.image = reader.result;
  }

  public onShowOptions = (show: boolean) => this.showBackdrop = show;

  public copyImageValues = () =>
  {
    if (this.onCopy || this.onPaste) return;
    // console.log("values copied");
    // return;
    this.cameraInfoService.onCopyImageValue(this.imageInfo);
    // this.setTosterAlert("dark", ['copied image Values']);
    // this.photoValuesBeenCopiedOrPasted();
    this.photoValBeenCopied();
    this.showBackdrop = false;
  }

  public pastImageValues = () =>
  {
    if (this.onCopy || this.onPaste) return;
    if (this.viewOnly) return this.showBackdrop = false;
    if (!this.cameraInfoService.copiedImageInfo) return;
    console.log("pasting values");

    if (this.cameraInfoService.copiedImageInfo)
    {
      this.imageInfo.caption = this.cameraInfoService.copiedImageInfo.caption;
      this.imageInfo.camera = this.cameraInfoService.copiedImageInfo.camera;
      this.imageInfo.lense = this.cameraInfoService.copiedImageInfo.lense;
      this.imageInfo.isoRate = this.cameraInfoService.copiedImageInfo.isoRate;
      this.imageInfo.f_stop = this.cameraInfoService.copiedImageInfo.f_stop;
      this.imageInfo.sutterSpeed = this.cameraInfoService.copiedImageInfo.sutterSpeed;
      this.imageInfo.overExpose = this.cameraInfoService.copiedImageInfo.overExpose;
      this.imageInfo.avg_light_shadow = this.cameraInfoService.copiedImageInfo.avg_light_shadow;
      this.imageInfo.film = this.cameraInfoService.copiedImageInfo.film;
    }
    // this.photoValuesBeenCopiedOrPasted();
    this.photoValBeenPasted();
    this.showBackdrop = false;
  }

  private photoValBeenCopied = () =>
  {
    this.onCopy = true;
    setTimeout(() => { this.onCopy = false; }, 2000)
  }

  private photoValBeenPasted = () =>
  {
    this.onPaste = true;
    setTimeout(() => { this.onPaste = false; this.cameraInfoService.onResteImageValue(); }, 2000)
  }

  // private photoValuesBeenCopiedOrPasted1 = () =>
  // {
  //   // this.onCopyOrPasteValues = true;
  //   // setTimeout(() => { this.onCopyOrPasteValues = false }, 2000);
  // }

  public addCameras = () =>
  {
    console.log("adding cameras", this.cameras)
    this.cameraInfoService.addNewCameras(this.cameras);
  }

  public addfilms = () =>
  {
    console.log("adding films", this.films)
    this.cameraInfoService.addNewfilms(this.films);
  }




}



























  // public readonly films: object = {
  //   Kodak: [
  //     "portra 160", "portra 400", "portra 800", "ektar 100", "Ultramax 400",
  //     "Gold 200", "Pro Image 100", "ColorPlus 200", "Ektachrome E100", "Ultramax 400",
  //     "T-MAX P3200", "T-MAX 400", "T-MAX 100", "Tri-X"
  //   ],
  //   Lomography: [
  //     "Fantome Kino", "Earl GreyOrca", "Lady Grey", "Potsdam 100", "Berlin 400", "Color Negative 100",
  //     "Color Negative 400", "Color Negative 800", "Color Tiger  200", "Redscale XR 50-200", "LomoChrome Metropolis",
  //     "LomoChrome Purple XR", "Lobster Redscale", "XPro 200", "Peacock X-Pro"
  //   ],
  //   ADOX: ["CMS 20 II PRO", "HR-50", "IR-HR PRO 50", "CHS 100 II", "Silvermax", "SCALA 50", "SCALA 160"],
  //   Argenti: ["Copex Rapid", "APX 100", "APX 400", "Nanotomic X", "Scale-X", "PAN-X", "ARF+ Reporter Film Plus"],
  //   Bergger: ["Pancro 400"],
  //   CatLABS: ["X FILM 80", "X FILM 320"],
  //   Cinestill: ["bwXX", "50D", "800T"],
  //   Dubblefilm: ["Apollo", "Bubblegum", "Jelly", "Pacific", "Solar", "Stereo"],
  //   Ferrania: ["P30"],
  //   Foma: ["FOMAPAN 100 'Classic", "FOMAPAN 200 'Creative", "FOMAPAN 400 'Creative", "RETRO PAN 320 'Soft", "FOMAPAN R 100"],
  //   FOTOIMPEX: ["CHM 100", "CHM 400"],
  //   FUJIFILM: ["Neopan ACROS 100 II", "Neopan 400CN", "FujiColor 100", "FujiColor Superia Premium 400", "FujiColor C200",
  //     "FujiColor Superia X-tra 400", "Fujicolor Pro 160NS", "Fujicolor Pro 400H", "Fujichrome Velvia 100", "Fujichrome Velvia 100F",
  //     "Fujichrome Provia 100F"],
  //   HOLGA: ["400"],
  //   ILFORD: ["Pan 100", "Pan 400", "Pan F Plus", "FP4 Plus", "HP5 Plus", "DELTA 100", "DELTA 400", "DELTA 3200"],
  //   ORTHO_PLUS: ["SFX 200", "XP2 Super"],
  //   JCH: ["Streetpan 400"],
  //   Kentmere: ["PAN 100", "PAN 400"],
  //   KONO: ["Donau 6", "Kolorit 125 Tungsten", "Kolorit 400 Tungsten", "Rotwild 400", "ALiEN 200",
  //     "UFO 200", "KATZ 200", "LUFT 200", "LIEBE 200", "WINTERMÄRCHEN 200"],
  //   Oriental: ["SEAGULL 100", "SEAGULL 400"],
  //   Rera: ["Rera Pan 400", "Chrome 100"],
  //   Revolog: ["Rasp", "Volvox", "Texture", "Plexus", "Laser", "Streak", "Tesla I", "Tesla II", "Kosmos", "Kolor", "nm 460", "nm 600"],
  //   Rollei: ["RPX 25", "RPX 100", "RPX 400", "Ortho Plus", "Retro 80S", "SuperPan 200", "Retro 400S", "Infrared 400", "Blackbird", "Redbird", "Redbird"],
  //   Shanghai: ["GP3 100 PAN"],
  //   Silberra: ["S25", "Orta 50", "Orta 100", "Pan 50/Ultima 50", "Pan 160/Ultima 160", "Pan 200/Ultima 200",
  //     "Ultima 100", "U200", "U400", "Cinema UN54", "Cinema+ 74N", "Cinema+ 75N", "Cinema 52XX"],
  //   SPUR: ["DSX", "UR", "Ultra R 800"],
  //   Street_Candy: ["ATM 400"],
  //   Svema_Astrum: ["МЗ-3 (MZ-3)", "H-64 (FN-64)", "Foto-100", "Foto-200", "Foto-400", "A-2SH", "NK-2", "Color"],
  //   Ultrafine: ["Xtreme 100", "Xtreme 400"],
  //   Yodica: ["Antares", "Andromeda", "Atlas", "Pegasus", "Polaris", "Sirio", "Vega"]


  // }
