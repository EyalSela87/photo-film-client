export class ImageInfo
{
    caption: string;
    camera: string;
    lense: string;
    isoRate: string;
    f_stop: string;
    sutterSpeed: string;
    overExpose: string;
    avg_light_shadow: string;
    film: string;
    url: string;

    constructor(caption: string, camera: string, lense: string, isoRate: string, f_stop: string,
        sutterSpeed: string, overExpose: string, avg_light_shadow: string, film: string, url?: string)
    {
        this.caption = caption;
        this.camera = camera;
        this.lense = lense;
        this.isoRate = isoRate;
        this.f_stop = f_stop;
        this.sutterSpeed = sutterSpeed;
        this.overExpose = overExpose;
        this.avg_light_shadow = avg_light_shadow;
        this.film = film;
        this.url = url || null;
    }
}