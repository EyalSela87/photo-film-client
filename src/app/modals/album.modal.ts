import { ImageInfo } from './imageInfo.modal';

export class Album
{
  id: number;
  name: string;
  images: ImageInfo[];
  coverImgIndex: number;

  constructor(name: string, images: ImageInfo[], coverImgIndex: number, id: number)
  {
    this.name = name;
    this.images = images;
    this.coverImgIndex = coverImgIndex;
    this.id = id ? id : null;
  }
}
