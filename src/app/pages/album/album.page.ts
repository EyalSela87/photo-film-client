import { Component, OnInit, OnDestroy } from '@angular/core';
// import { Router, ActivatedRoute, Params } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
// import { NavController, ToastController } from '@ionic/angular';
import { NavController, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';

import { UsersService } from '../../services/users.service';
import { AlbumsService } from '../../services/albums.service';
import { LogsService } from '../../services/logs.service';
import { ImageInfo } from '../../modals/imageInfo.modal';

import { ModalController } from '@ionic/angular';
import { ModalComponent } from '../../components/modal/modal.component';

@Component({
  selector: 'app-album',
  templateUrl: './album.page.html',
  styleUrls: ['./album.page.scss'],
})
export class AlbumPage implements OnInit, OnDestroy {
  private imagesUpdateSub: Subscription;

  constructor(
    private userService: UsersService,
    public albumsService: AlbumsService,
    private logsService: LogsService,
    private navCtrl: NavController,
    private activeRoute: ActivatedRoute,
    private toastController: ToastController,
    private modalCtrl: ModalController
  ) {}

  ngOnInit() {
    this.logsService.onError.subscribe((errors: string[]) => {
      this.setTosterAlert('dark', errors);
      this.modalCtrl.dismiss();
    });
  }

  ionViewWillEnter() {
    if (!this.userService.isUserLogged()) this.navCtrl.navigateBack(['/login']);
    if (!this.albumsService.albums) this.navCtrl.navigateBack(['/main']);
  }

  ionViewWillLeave() {}

  async setTosterAlert(
    color: string,
    errors: string[],
    mili_sec: number = 3000
  ) {
    // popup alets with following errors
    const toast = await this.toastController.create({
      message: errors.join('\n\n'),
      position: 'top',
      color: color,
      duration: mili_sec,
    });
    toast.present();
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: ModalComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        albumInfo: this.albumsService.currentAlbum,
      },
    });
    return await modal.present();
  }

  async presentModalViewOnly(image: ImageInfo) {
    const modal = await this.modalCtrl.create({
      component: ModalComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        albumInfo: this.albumsService.currentAlbum,
        viewOnly: true,
        imageInfo: image,
      },
    });
    return await modal.present();
  }

  public onAddNewImage = () => {
    this.presentModal();
  };

  public onViewImageDetails = (image: ImageInfo) => {
    console.log('(onViewImageDetails)', image);
    this.presentModalViewOnly(image);
  };

  public onBackButton = () => this.navCtrl.navigateBack(['/main']);

  ngOnDestroy() {}
}
