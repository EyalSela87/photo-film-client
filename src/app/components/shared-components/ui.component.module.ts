import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';


import { VisualSquareComponent } from '../visual-square/visual-square.component';
import { BackdropComponent } from '../backdrop/backdrop.component';
import { ModalComponent } from '../modal/modal.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [VisualSquareComponent, BackdropComponent],
    exports: [VisualSquareComponent, BackdropComponent],
    imports: [CommonModule, IonicModule, FormsModule]
})

export class UiComponentsModules { }