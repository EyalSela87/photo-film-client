export class UserInfo
{
    id: number;
    name: string;
    email: string;
    password: string;
    verifyPassword: string;

    constructor(name: string, email: string, password: string, verifyPassword: string, id?: number)
    {
        this.name = name;
        this.email = email;
        this.password = password;
        this.verifyPassword = verifyPassword;
        this.id = id ? id : null;
    }
}