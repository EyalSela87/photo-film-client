import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UserInfo } from '../modals/userInfo.modal';
import { Response } from '../modals/response.modal';
import { LogsService } from './logs.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UsersService
{
  readonly BASE_URL: string = 'https://404040.co.il/firumu2/users';
  readonly STORAGE_USER_KEY: string = "STORAGE_USER_KEY";
  // readonly BASE_URL: string = 'http://localhost:3000/users';
  // readonly BASE_URL: string = "https://10.0.2.2:3000/users";

  private user: UserInfo = null;
  // private user: UserInfo = new UserInfo('Eyal Sela', 'eyal@test.com', null, null, 1);
  public onAuthChanged = new Subject<void>();

  constructor(private http: HttpClient, private LogService: LogsService)
  {
    this.checkForUserOnStorage();
  }

  public checkForUserOnStorage = () =>
  {
    if (!localStorage.getItem(this.STORAGE_USER_KEY)) return;
    const storageUser: object = JSON.parse(localStorage.getItem(this.STORAGE_USER_KEY));
    if (storageUser === null) return;

    this.user = new UserInfo(storageUser['name'], storageUser['email'], null, null, storageUser['id']);
    this.onAuthChanged.next();
  }

  public loginUser = (user: UserInfo) =>
  {
    const data = {
      email: user.email,
      password: user.password,
    };

    console.log('loginUser', 1);

    this.http.post<Response>(this.BASE_URL + '/login', data).subscribe(
      (res) =>
      {
        console.log('loginUser', 1, res);
        if (res.succeed === undefined)
          return this.LogService.throwError([
            'somting went wrong',
            'pease try again later',
          ]);
        if (!res.succeed)
          return this.LogService.throwError(res.error.split(','));

        this.user = new UserInfo(res.data['name'], res.data['email'], null, null, res.data['id']);
        localStorage.setItem(this.STORAGE_USER_KEY, JSON.stringify(this.user));
        this.onAuthChanged.next();
      },
      (err) =>
      {
        console.log(err);
        this.LogService.throwError(['email or password is not match']);
        // this.LogService.throwError(err['error']['error'].split(','));
      }
    );
  };

  public logoutUser = () =>
  {
    localStorage.removeItem(this.STORAGE_USER_KEY);
    this.user = null;
  }

  public isUserLogged = (): boolean => this.user !== null;

  public createNewUser = (user: UserInfo) =>
  {
    const data = {
      name: user.name,
      email: user.email,
      password: user.password,
      verifyPassword: user.verifyPassword,
    };

    this.http.post<Response>(this.BASE_URL + '/create', data).subscribe(
      (res) =>
      {
        if (!res.succeed) return this.LogService.throwError([res.error]);
        this.user = new UserInfo(res.data['name'], res.data['email'], null, null, res.data['id']);
        this.onAuthChanged.next();
      },
      (err) =>
      {
        this.LogService.throwError(err['error']['error'].split(','));
      }
    );
  };

  public resetUserPassword = (email: string) =>
  {
    console.log("sending reset email to " + email);

    const data = { email: email };

    this.http.post<Response>(this.BASE_URL + '/resetPass', data)
      .subscribe(result =>
      {
        console.log("result", result)
        this.LogService.throwError(["email has been sent"]);
      }, err => this.LogService.throwError(err['error']['error'].split(',')));
  }

  public getUserId = (): number => (this.user ? this.user.id : null);

  public test = () =>
  {
    this.http.get('https://jsonplaceholder.typicode.com/posts')
      .subscribe((result) =>
      {
        this.LogService.throwError([result[0]['title']]);
      });
  };
}
