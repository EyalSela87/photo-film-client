import { Component, OnInit, Input } from '@angular/core';
import { Album } from '../../modals/album.modal';
import { ImageInfo } from '../../modals/imageInfo.modal';

@Component({
  selector: 'app-visual-square',
  templateUrl: './visual-square.component.html',
  styleUrls: ['./visual-square.component.scss'],
})
export class VisualSquareComponent implements OnInit
{

  @Input() addButton: boolean = false;
  @Input() info: Album;
  @Input() addImage: string;

  @Input() albumInfo: Album = null;
  @Input() imageInfo: ImageInfo = null;


  public bgImage: string;
  public imageSize: { width: number, height: number };

  constructor() { }

  ngOnInit()
  {
    if (this.addButton)
    {
      this.bgImage = "assets/images/add-icon.png"
    }

    else
    {
      this.bgImage = "assets/images/blank-pink.png"
    }
  }

  getCameraName = (): string =>
  {
    let str: string;

    if (this.imageInfo.camera.length <= 10)
      str = this.imageInfo.camera;
    else str = this.imageInfo.camera.substring(0, 8) + "...";

    return str;
  }

}
